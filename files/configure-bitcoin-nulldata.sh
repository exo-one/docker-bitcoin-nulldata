#!/bin/bash

( cd /home/$SVST_USER/bitcoin-nulldata && \
  ./autogen.sh && \
  export BDB_PREFIX=/usr/local/BerkeleyDB.4.8 && \
    ./configure CPPFLAGS="-I${BDB_PREFIX}/include/ -O2" \
    LDFLAGS="-L${BDB_PREFIX}/lib/" ) || (echo "Config failed!" && exit 1)
