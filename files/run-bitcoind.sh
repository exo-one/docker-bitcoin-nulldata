#!/bin/bash

cat <<END > /home/$SVST_USER/.bitcoin/bitcoin.conf
disablewallet=0
printtoconsole=1
server=1
rest=1
rpcuser=bitcoinrpc
rpcpassword=YwdRRcTVR7auH0xWukWUd5qk77t2rxP6FKJlk1YL0YIM
rpcallowip=172.17.0.0/16
dbcache=2048
par=2
checklevel=0
END

BITCOIND=/home/$SVST_USER/bitcoin-nulldata/src/bitcoind

if [ ! -f $BITCOIND ]; then
  echo "Could not find '$BITCOIND'"
  exit 1
fi

$BITCOIND $@