#!/bin/bash

if [ $# -lt 1 ]; then
  echo "Usage: $(basename $0) <cmd> [args ...]"
  exit 1
fi

docker ps 2>&1 | grep -q -e 'svst$'

if [ ${PIPESTATUS[1]} -ne 0 ];then
  echo "Container with name 'svst' is not running"
  exit 1
fi

docker exec -it svst "$@"
