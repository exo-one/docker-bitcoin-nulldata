FROM ubuntu:16.04
MAINTAINER Max Kaye <m@xk.io>

ARG USER_ID
ARG GROUP_ID

ENV SVST_USER svst
ENV TERM xterm

# Required for bsdmainutils which contains hexdump
RUN dpkg --add-architecture i386

RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 575159689BEFB442
RUN echo 'deb http://download.fpcomplete.com/ubuntu xenial main'| tee /etc/apt/sources.list.d/fpco.list

RUN apt-get update && \
    apt-get install -y \
      wget git stack \
      build-essential libtool autotools-dev autoconf pkg-config libssl-dev \
      libboost-all-dev libqt4-dev libprotobuf-dev protobuf-compiler \
      libqrencode-dev libminiupnpc-dev bsdmainutils libevent-dev sudo nano


# add user with specified (or default) user/group ids
ENV USER_ID ${USER_ID:-1000}
ENV GROUP_ID ${GROUP_ID:-1000}
RUN groupadd -g 1001 postgres
RUN useradd  -u 1001 -g postgres -s /bin/bash postgres
RUN groupadd -g ${GROUP_ID} $SVST_USER
RUN useradd  -u ${USER_ID} -g $SVST_USER -s /bin/bash -m -d /home/$SVST_USER $SVST_USER


WORKDIR /root

RUN wget http://download.oracle.com/berkeley-db/db-4.8.30.NC.tar.gz

ADD files/build-bdb.sh /root
RUN /root/build-bdb.sh

RUN wget https://dist.ipfs.io/go-ipfs/v0.4.4/go-ipfs_v0.4.4_linux-amd64.tar.gz
RUN tar xzf go-ipfs_v0.4.4_linux-amd64.tar.gz && \
    mv go-ipfs/ipfs /usr/local/bin && mkdir -p /svst-data/.ipfs && \
    ln -s /svst-data/.ipfs /home/$SVST_USER/.ipfs

RUN wget https://storage.googleapis.com/golang/go1.7.4.linux-amd64.tar.gz
RUN tar xzf go1.7.4.linux-amd64.tar.gz && mv go /usr/local/go && \
    ln -s /usr/local/go/bin/go /usr/local/bin/go

ENV HOME /home/$SVST_USER

RUN chown $SVST_USER:$SVST_USER -R /home/$SVST_USER
RUN echo "$SVST_USER:$SVST_USER" | chpasswd && adduser $SVST_USER sudo && \
    echo '%sudo ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

RUN mkdir -p /svst-data


RUN mkdir -p /svst-data/postgresql && \
    chown postgres:postgres /svst-data/postgresql && \
    ln -s /svst-data/postgresql /var/lib/postgresql

RUN apt-get install -y postgresql-9.5 postgresql-client-9.5 libpq-dev

RUN echo "host all  all 0.0.0.0/0  md5" >> /etc/postgresql/9.5/main/pg_hba.conf
RUN echo "listen_addresses='*'" >> /etc/postgresql/9.5/main/postgresql.conf

USER $SVST_USER
WORKDIR /home/$SVST_USER

# These are to use once you've started an interactive session
ADD files/configure-bitcoin-nulldata.sh /home/$SVST_USER
ADD files/build-bitcoin-nulldata.sh /home/$SVST_USER
ADD files/run-bitcoind.sh /home/$SVST_USER
ADD files/run-postgres-daemon.sh /home/$SVST_USER

RUN ln -s /svst-data/.bitcoin /home/$SVST_USER/.bitcoin
RUN ln -s /svst-data/.stack /home/$SVST_USER/.stack

# For Bitcoin
EXPOSE 8332 8333 18332 18333
# For IPFS
EXPOSE 4001 5001 8080
