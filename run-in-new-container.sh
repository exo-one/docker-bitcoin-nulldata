#!/bin/bash

if [ "$BITCOIN_NULLDATA_PATH" = "" ];then
  echo "BITCOIN_NULLDATA_PATH not defined"
  exit 1
fi

if [ "$SVST_HASKELL_PATH" = "" ]; then
  echo "SVST_HASKELL_PATH not defined"
  exit 1

fi

if [ $# -lt 2 ]; then
  echo "Usage: $(basename $0) <image hash> cmd [args...]"
  exit 1
fi

HASH=$1
CMD=$2
shift
shift
# Rest of args are now bitcoind args

docker run -it --rm \
    --name svst \
    -v svst-data:/svst-data  \
    -v "$BITCOIN_NULLDATA_PATH":/home/svst/bitcoin-nulldata \
    -v "$SVST_HASKELL_PATH":/home/svst/svst-haskell \
    -p 8333:8333 \
    -p 127.0.0.1:8332:8332 \
    "$HASH" \
    $CMD $@
