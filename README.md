# Dockerisation for bitcoin-nulldata repo

## Set up environment variables

Set `BITCOIN_NULLDATA_PATH` to path of `bitcoin-nulldata` repo. e.g.

    export BITCOIN_NULLDATA_PATH=/home/ubuntu/bitcoin-nulldata

## Building Docker image

    $ docker build .

This will produce output like:

    [...]
     ---> a1ff3e274338
    Step 24 : EXPOSE 8332 8333 18332 18333
     ---> Using cache
     ---> c1ef315bf180
    Successfully built c1ef315bf180

Remember the _image hash_ after `Successfully built`. You will need it to
build `bitcoind`

## Building `bitcoind`

There are two scripts, one to configure and one to build

- `./configure-bitcoin-nulldata-sh`
- `./build-bitcoin-nulldata.sh`

These map the repo path a _temporary_ running container and allow you to build
`bitcoin-nulldata` _inside_ the running container.  However, since it is mapped
in the results are not forgotten once you stop (or even remove) the container.

     $ ./configure-bitcoin-nulldata.sh <image hash>
     $ ./build-bitcoin-nulldata.sh <image hash>

## Running `bitcoind` inside temporary new container

Either use `tmux` or `screen` to run this in background

     $ ./run-bitcoind-in-container.sh <image hash>

The container will be named: `svst`.

This is important if you wish to run `bitcoin-cli`. (See below)

## Running `bitcoin-cli`

This will only work once you have run `./run-bitcoind-in-container.sh` (and it
is running in background in `tmux` or `screen`)

It uses `docker exec` which allows you to run a process in a running container.

    $ ./run-bitcoin-cli.sh

# Running `bash` in running container

    $ ./run-bash.sh
